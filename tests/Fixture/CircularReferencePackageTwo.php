<?php

namespace Weeny\Core\PackageManager\Tests\Fixture;

use Weeny\Contract\Collection\StringCollectionInterface;
use Weeny\Contract\Package\DependencedPackageInterface;
use Weeny\Contract\Package\PackageInterface;
use Weeny\Lib\Collection\StringCollection;

class CircularReferencePackageTwo implements PackageInterface, DependencedPackageInterface
{

    /**
     * @inheritDoc
     */
    public function getDependencyPackageNames(): StringCollectionInterface
    {
        return new StringCollection(CircularReferencePackageOne::class);
    }

    /**
     * @inheritDoc
     */
    public function getShortName(): string
    {
        return 'CircularReferencesPackageTwo';
    }
}