<?php
namespace Weeny\Core\PackageManager\Tests\Fixture;

use Weeny\Contract\Package\PackageInterface;

class SimplePackageTwo implements PackageInterface
{

    /**
     * @inheritDoc
     */
    public function getShortName(): string
    {
        return 'SimplePackageTwo';
    }
}