<?php

namespace Weeny\Core\PackageManager\Tests;

use PHPUnit\Framework\TestCase;
use Weeny\Contract\Exceptions\CircularReferenceWatcherExceptionInterface;
use Weeny\Contract\Package\DependencyResolverInterface;
use Weeny\Contract\Package\Exceptions\PackageInitializationExceptionInterface;
use Weeny\Contract\Package\Exceptions\PackageNotFoundExceptionInterface;
use Weeny\Core\PackageManager\DependencyResolver;
use Weeny\Core\PackageManager\Tests\Fixture\CircularReferencePackageOne;
use Weeny\Core\PackageManager\Tests\Fixture\DependencedPackageTwo;
use Weeny\Core\PackageManager\Tests\Fixture\PackageWithUnknownDependencedClass;
use Weeny\Core\PackageManager\Tests\Fixture\ProblemInitializinOne;
use Weeny\Core\PackageManager\Tests\Fixture\ProblemInitializinTwo;
use Weeny\Core\PackageManager\Tests\Fixture\SimplePackageOne;
use Weeny\Core\PackageManager\Tests\Fixture\SimplePackageTwo;
use Weeny\Lib\CircularWatcher\CircularReferenceWatcher;

class DependencyResolverTest extends TestCase
{
    /**
     * @var DependencyResolverInterface
     */
    private $loader;

    public function setUp()
    {
        parent::setUp();
        $this->loader = new DependencyResolver(new CircularReferenceWatcher());
    }

    public function testSimpleLoad() {
        $collection = $this->loader->resolveDependencies(new SimplePackageOne(), new SimplePackageTwo());
        $this->assertCount(2, $collection);
        $this->assertEquals('SimplePackageOne', $collection[0]->getShortName());
        $this->assertEquals('SimplePackageTwo', $collection[1]->getShortName());
    }

    public function testSimpleDoubleLoad() {
        $collection = $this->loader->resolveDependencies(new SimplePackageOne(), new SimplePackageOne());
        $this->assertCount(1, $collection);
        $this->assertTrue($collection->contains(new SimplePackageOne()));
    }

    public function testLoadWithDependences() {
        $collection = $this->loader->resolveDependencies(new DependencedPackageTwo());
        $this->assertCount(4, $collection);
        $this->assertEquals('SimplePackageOne', $collection[0]->getShortName());
        $this->assertEquals('SimplePackageTwo', $collection[1]->getShortName());
        $this->assertEquals('DependencedPackageOne', $collection[2]->getShortName());
        $this->assertEquals('DependencedPackageTwo', $collection[3]->getShortName());
    }

    public function testPackageNotExists() {
        try {
            $this->loader->resolveDependencies(new PackageWithUnknownDependencedClass());
            $this->markAsRisky();
        } catch (PackageNotFoundExceptionInterface $e) {
            $this->assertEquals('SomeUnexistsPackageClassName', $e->getPackageName());
        }
    }

    public function testWithProblemOfInitializePackageInstanse() {
        try {
            $this->loader->resolveDependencies(new ProblemInitializinOne());
            $this->markAsRisky();
        } catch (PackageInitializationExceptionInterface $e) {
            $this->assertEquals(\ReflectionClass::class, $e->getPackageName());
        }

        try {
            $this->loader->resolveDependencies(new ProblemInitializinTwo());
            $this->markAsRisky();
        } catch (PackageInitializationExceptionInterface $e) {
            $this->assertEquals(\DateTime::class, $e->getPackageName());
        }
    }

    public function testCircularReferences() {
        $this->expectException(CircularReferenceWatcherExceptionInterface::class);
        $this->loader->resolveDependencies(new CircularReferencePackageOne());
    }
}