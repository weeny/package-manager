<?php
namespace Weeny\Core\PackageManager\Tests\Collection;

use Weeny\Contract\Package\PackageInterface;
use Weeny\Core\PackageManager\Collection\PackageCollection;
use Weeny\Core\PackageManager\Tests\Fixture\DependencedPackageOne;
use Weeny\Core\PackageManager\Tests\Fixture\DependencedPackageTwo;
use Weeny\Core\PackageManager\Tests\Fixture\SimplePackageOne;
use Weeny\Core\PackageManager\Tests\Fixture\SimplePackageTwo;
use Weeny\Lib\Collection\Tests\TypedCollectionTest;

class PackageCollectionTest extends TypedCollectionTest
{


    public function dataProviderNotEmptyCollection()
    {
        $elements = [new SimplePackageOne(), new SimplePackageTwo(), new DependencedPackageOne()];
        return [
            [
                new PackageCollection(...$elements),
                $elements,
                new DependencedPackageTwo()
            ]
        ];
    }

    public function dataProviderForCreatePositive()
    {
        return [
            [
                PackageCollection::class,
                [new SimplePackageOne(), new SimplePackageTwo(), new DependencedPackageOne()]
            ]
        ];
    }

    public function dataProviderForCreateNegative()
    {
        return [
            [
                PackageCollection::class,
                [new SimplePackageOne(), 1, new DependencedPackageOne()]
            ]
        ];
    }

    public function dataProviderForManipulateNegative()
    {
        return [
            [
                new PackageCollection(new SimplePackageOne(), new SimplePackageTwo(), new DependencedPackageOne()),
                1
            ]
        ];
    }

    public function testFilterInstanceOf() {
        $collection = new PackageCollection(
            new SimplePackageOne(),
            new SimplePackageTwo()
        );

        $resultOne = $collection->filterInstanceOf(SimplePackageOne::class);
        $this->assertCount(1, $resultOne);
        $this->assertInstanceOf(SimplePackageOne::class, $resultOne->current());

        $resultTwo = $collection->filterInstanceOf(PackageInterface::class);
        $this->assertCount(2, $resultTwo);
        $this->assertEquals($collection, $resultTwo);
    }
}