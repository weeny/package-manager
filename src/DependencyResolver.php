<?php
namespace Weeny\Core\PackageManager;

use Weeny\Contract\Exceptions\CircularReferenceWatcherExceptionInterface;
use Weeny\Contract\Exceptions\WeenyExceptionInterface;
use Weeny\Contract\Package\DependencedPackageInterface;
use Weeny\Contract\Package\DependencyResolverInterface;
use Weeny\Contract\Package\Exceptions\PackageInitializationExceptionInterface;
use Weeny\Contract\Package\PackageCollectionInterface;
use Weeny\Contract\Package\PackageInterface;
use Weeny\Core\PackageManager\Collection\PackageCollection;
use Weeny\Core\PackageManager\Exceptions\PackageInitializationException;
use Weeny\Core\PackageManager\Exceptions\PackageNotFoundException;
use Weeny\Lib\CircularWatcher\CircularReferenceWatcherInterface;

class DependencyResolver implements DependencyResolverInterface
{

    /**
     * @var CircularReferenceWatcherInterface
     */
    protected $circularWatcher;

    public function __construct(CircularReferenceWatcherInterface $circularWatcher)
    {
        $this->circularWatcher = $circularWatcher;
    }

    /**
     * @inheritDoc
     */
    public function resolveDependencies(PackageInterface ...$packages): PackageCollectionInterface
    {
        $collection = new PackageCollection();

        foreach ($packages as $package) {
            $this->addPackageToCollection($package, $collection);
        }

        return $collection;
    }

    /**
     * @param PackageInterface $package
     * @param PackageCollectionInterface $collection
     * @throws CircularReferenceWatcherExceptionInterface
     * @throws PackageInitializationExceptionInterface
     * @throws WeenyExceptionInterface
     */
    protected function addPackageToCollection(PackageInterface $package, PackageCollectionInterface $collection)
    {
        if ( $collection->contains($package) ) {
            return;
        }

        if ( $package instanceof DependencedPackageInterface ) {
            $this->addDependencedPackage($package, $collection);
        }
        $collection[] = $package;
    }

    /**
     * @param DependencedPackageInterface $package
     * @param PackageCollectionInterface $collection
     * @throws CircularReferenceWatcherExceptionInterface
     * @throws PackageInitializationExceptionInterface
     * @throws WeenyExceptionInterface
     */
    protected function addDependencedPackage(DependencedPackageInterface $package, PackageCollectionInterface $collection)
    {

        $this->circularWatcher->openContext(get_class($package));
        $dependencyNames = $package->getDependencyPackageNames();

        foreach ( $dependencyNames as $packageName ) {
            if ( !class_exists($packageName) ) {
                throw new PackageNotFoundException(
                    $packageName,
                    sprintf(
                        'The package %s class not found',
                        $packageName
                    )
                );
            }

            try {
                $packageInstance = new $packageName();
            } catch (\Throwable $e) {
                throw new PackageInitializationException(
                    $packageName,
                    sprintf(
                        $e->getMessage(),
                        $packageName
                    ),
                    0,
                    $e
                );
            }

            if ( !($packageInstance instanceof PackageInterface) ) {
                throw new PackageInitializationException(
                    $packageName,
                    sprintf(
                        'Package of class %s isn\'t implements %s',
                        $packageName,
                        PackageInterface::class
                    )
                );
            }

            if ( $collection->contains($packageInstance) ) {
                continue;
            }

            if ( $packageInstance instanceof DependencedPackageInterface ) {
                $this->addDependencedPackage($packageInstance, $collection);
            }
            $collection[] = $packageInstance;
        }
        $this->circularWatcher->closeContext(get_class($package));
    }
}