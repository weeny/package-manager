<?php

namespace Weeny\Core\PackageManager\Exceptions;

use Weeny\Contract\Package\Exceptions\PackageNotFoundExceptionInterface;

class PackageNotFoundException extends PackageInitializationException implements PackageNotFoundExceptionInterface
{

}