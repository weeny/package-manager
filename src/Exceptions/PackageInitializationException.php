<?php

namespace Weeny\Core\PackageManager\Exceptions;

use Throwable;
use Weeny\Contract\Package\Exceptions\PackageInitializationExceptionInterface;

class PackageInitializationException extends \Exception implements PackageInitializationExceptionInterface
{

    protected $packageName;

    public function __construct(string $packageName, string $message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->packageName = $packageName;
    }

    /**
     * @inheritDoc
     */
    public function getPackageName(): string
    {
        return $this->packageName;
    }

}