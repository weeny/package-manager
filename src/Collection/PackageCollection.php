<?php
namespace Weeny\Core\PackageManager\Collection;

use Weeny\Contract\Package\PackageCollectionInterface;
use Weeny\Contract\Package\PackageInterface;
use Weeny\Lib\Collection\AbstractCollection;

class PackageCollection extends AbstractCollection implements PackageCollectionInterface
{

    public function __construct(PackageInterface ...$elements)
    {
        parent::__construct(...$elements);
    }

    public function current(): PackageInterface
    {
        return parent::current();
    }

    public function offsetGet($offset): PackageInterface
    {
        return parent::offsetGet($offset);
    }

    /**
     * @inheritDoc
     */
    protected function checkType($element, string $message): void
    {
        if ( !($element instanceof PackageInterface) )
        {
            throw new \TypeError(sprintf($message, PackageInterface::class));
        }
    }

    public function contains($value): bool
    {
        $this->checkType($value, 'Argument must be of type %s');

        $className = get_class($value);
        foreach ($this->elements as $element) {
            if (get_class($element) == $className) {
                return true;
            }
        }
        return false;
    }

    public function removeByIndex(int $index): ?PackageInterface {
        return parent::removeByIndex($index);
    }

    /**
     * @inheritDoc
     */
    public function pop(): PackageInterface {
        return parent::pop();
    }

    /**
     * @inheritDoc
     */
    public function shift(): PackageInterface {
        return parent::shift();
    }

    public function unique(): void
    {
        $exists = [];
        $result = [];
        foreach ($this->elements as $package) {
            $name = get_class($package);
            if ( in_array($name, $exists) ) {
                continue;
            }
            $exists[] = $name;
            $result[] = $package;
        }
        $this->elements = $result;
        $this->rewind();
    }

    /**
     * @inheritDoc
     */
    public function filterInstanceOf(string $className): PackageCollectionInterface
    {
        $collection = new PackageCollection();
        foreach ($this as $package) {
            if ($package instanceof $className) {
                $collection->push($package);
            }
        }
        return $collection;
    }
}